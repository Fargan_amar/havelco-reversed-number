'use strict'

const readLine = require('readline');

const ui = readLine.createInterface({
    input: process.stdin,
    output: process.stdout,
    terminal: false
})

const main = () => {
    return new Promise((resolve, reject) => {
        ui.question(`Welcome to the Reversed Number Game, Please type 'start' for play the game or type 'exit' to exit from this game : `, (data) => {
            if (data.toLowerCase() !== 'start') {
                console.log('Have a Nice Day, BYE!')
                process.exit();
            }

            resolve(start())
        })        
    })
}

const txt1 = () => {
    return new Promise((resolve, reject) => {
        ui.question('Please Type the number : ', data => {
            resolve(data)
        })
    })
}

const reversedNum = num => {
    return new Promise((resolve, reject) => { 
        const result = parseFloat(num.toString().replace(/\s/g, '').split('').reverse().join(''))

        resolve(result)
    })
} 

const sumNumber = num => {
    return new Promise((resolve, reject) => {
        const arr = num.toString(10).split('').map(Number)  
        const sum = arr.reduce((a, b) => a + b)

        resolve(sum)
        
    })
}

const start = async() => {
    const number = await txt1()
    const reverse = await reversedNum(number)
    const result = await sumNumber(reverse)

    console.log(result)

    main()
}

main()
